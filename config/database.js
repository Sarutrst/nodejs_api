let mysql = require("mysql");

let PORT = 3306;
let config = {
  host: "localhost",
  port: PORT,
  user: "root",
  password: "root",
  database: "nodejs_api",
};

let dbCon = mysql.createConnection(config);
dbCon.connect(function (err) {
  if (err) {
    console.error("error connecting: " + err.stack);
    return;
  }
  console.log("Connect DB Successful.");
});

module.exports = dbCon;
