const database = require('../config/database')

exports.welcome = (req, res) => {
  return res.send({
    error: false,
    message: "Welcome to RESTful API",
    created_by: "icesu",
  });
};

exports.getbook = (req, res) => {
  database.query("SELECT * FROM Books", (error, results, field) => {
    if (error) throw error;

    if (results === undefined || results.length == 0) {
      return res.status(400).json({
        error: false,
        data: results,
        message: "Books Table is Empty",
      });
    } else {
      return res.status(200).json({
        error: false,
        data: results,
        message: "Successfully get Data From Books",
      });
    }
  });
};

exports.getBookById = (req, res) => {
  try {
    let id = req.params.id;
    if (!id || id.length <= 0) {
      return res.status(400).json({
        error: true,
        message: "Please Provide Book id",
      });
    }

    database.query(
      "SELECT * FROM Books WHERE id = ?",
      id,
      (error, results, field) => {
        if (error) throw error;

        if (!results || results.length == 0) {
          return res.status(404).json({
            error: true,
            message: "Book not found",
          });
        } else {
          return res.status(200).json({
            error: false,
            data: results,
          });
        }
      }
    );
  } catch (error) {
    return res.status(400).json({
      error: true,
      message: "Please Provide Book id",
    });
  }
};

exports.addBook = (req, res) => {
  let name = req.body.name;
  let author = req.body.author;
  if (!name) {
    return res.status(400).json({
      error: true,
      message: "Please Provide Book name.",
    });
  } else if (!author) {
    return res.status(400).json({
      error: true,
      message: "Please Provide Author.",
    });
  } else {
    database.query(
      "INSERT INTO Books (name,author) VALUES(?, ?)",
      [name, author],
      (error, results, field) => {
        if (error) throw error;

        return res.status(200).json({
          error: false,
          message: "Successfully added book",
        });
      }
    );
  }
};

exports.updateBookById = (req, res) => {
  let name = req.body.name;
  let author = req.body.author;

  let id = req.params.id;
  if (!id) {
    return res.status(400).json({
      error: true,
      message: "Please Provide Book id",
    });
  } else {
    if (!name) {
      return res.status(400).json({
        error: true,
        message: "Please Provide Book name.",
      });
    } else if (!author) {
      return res.status(400).json({
        error: true,
        message: "Please Provide Author.",
      });
    } else {
      database.query(
        "UPDATE Books SET name = ?, author = ? WHERE id = ?",
        [name, author, id],
        (error, results, field) => {
          if (error) throw error;

          if (results.changedRows === 0) {
            return res.status(400).json({
              error: true,
              message: "Book not found Or Same Data",
            });
          } else {
            return res.status(200).json({
              error: false,
              message: "Successfully updated book",
            });
          }
        }
      );
    }
  }
};

exports.deleteBookById = (req, res) => {
  let id = req.params.id;
  if (!id) {
    return res.status(400).json({
      error: true,
      message: "Please Provide Book id",
    });
  } else {
    database.query(
      "DELETE FROM Books WHERE id = ?",
      id,
      (error, results, field) => {
        if (error) throw error;

        if (results.affectedRows === 0) {
          return res.status(400).json({
            error: true,
            message: "Book not found",
          });
        } else {
          return res.status(200).json({
            error: false,
            message: "Successfully deleted book",
          });
        }
      }
    );
  }
};

