const express = require("express");
const bodyParser = require("body-parser");
const BookRouter = require('./routes/index');

let app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/api',BookRouter)

app.all('*', (req, res) => {
  return res.status(404).json({
    error: true,
    message: "404 Not Found this Page!",
  });
})

app.listen(3000, () => {
  console.log(`Server is runing on port 3000`);
});

module.exports = app;
