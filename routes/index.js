const express = require('express');
let router = express.Router();
let books = require('../controllers/books')

router.get('/',books.welcome)
router.get('/books',books.getbook)
router.get('/book/:id',books.getBookById)
router.post('/addbook',books.addBook)
router.put('/updatebook/:id',books.updateBookById)
router.delete('/deletebook/:id',books.deleteBookById)

module.exports = router